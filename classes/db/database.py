import pprint

from classes.db.properties import Properties
from classes.singleton import *
import os
import cv2
import numpy as np

IMAGE_DIRECTORY = "templates/"


class DataBase(object):
    __metaclass__ = Singleton
    pp = pprint.PrettyPrinter(indent=4)
    BASE = {}

    @classmethod
    def read(cls):
        gesture_dir = os.listdir(IMAGE_DIRECTORY)
        for images_dir_name in gesture_dir:
            prop = Properties()

            images_dir = os.listdir(IMAGE_DIRECTORY + images_dir_name)
            # print("images_dir->{}".format(images_dir))
            for img in images_dir:
                filepath = "{}/{}/{}".format(IMAGE_DIRECTORY, images_dir_name, img)
                # print(filepath)
                bin_image = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
                prop = DataBase.process_image(bin_image, prop)

            cls.BASE[images_dir_name] = prop
            # print("images_dir->{}".format(images_dir))

    @staticmethod
    def process_image(bin_image, prop):
        _, contour, _ = cv2.findContours(bin_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        c = max(contour, key=cv2.contourArea)

        x, y, w, h = cv2.boundingRect(c)
        prop._set_aspect_radio(w / float(h))
        # print(prop.aspect_radio)

        area = cv2.contourArea(c)
        prop._set_extent(area / float(w * h))
        # print(prop.extent)

        convex_hull = cv2.convexHull(c)
        hull_area = cv2.contourArea(convex_hull)
        prop._set_solidity(area / float(hull_area))
        # print(prop.solidity)

        prop._set_eq_diameter(np.math.sqrt(4 * area / np.math.pi))
        # print(prop.equivalent_diameter)

        _, _, angle = cv2.fitEllipse(c)
        prop._set_orientation(angle)
        # print(prop.orientation)

        return prop

    @classmethod
    def find_match(cls, prop):
        if not cls.BASE:
            return None
        results = {}
        for gesture_name in cls.BASE:
            template = cls.BASE[gesture_name]
            result = Properties.compare(template, prop)
            results[gesture_name] = result

        biggest_prob = max(results, key=results.get)
        # print(results.__str__())
        # print("biggest_prob:" + biggest_prob.__str__() + ", " + results[biggest_prob].__str__())
        if results[biggest_prob] >= 0.7:  # TODO: change when have worse lightning or whatever
            return biggest_prob
        else:
            return None

    @classmethod
    def print_database(cls):
        cls.pp.pprint(cls.BASE)



