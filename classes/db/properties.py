def set_property(prop, value):
    tup = (value, value)
    if isinstance(prop, tuple):
        minimum = prop[0]
        maximum = prop[1]
        if minimum > value:
            tup = (value, maximum)
        elif maximum < value:
            tup = (minimum, value)

    return tup


class Properties(object):
    def __init__(self):
        self.aspect_radio = None
        self.extent = None
        self.solidity = None
        self.equivalent_diameter = None
        self.orientation = None

    def __repr__(self):
        return "aspect_radio:{},extent:{},solidity:{},equivalent_diameter:{},orientation:{}".format(
            self.aspect_radio,
            self.extent,
            self.solidity,
            self.equivalent_diameter,
            self.orientation)

    def _set_aspect_radio(self, value):
        self.aspect_radio = set_property(self.aspect_radio, value)

    def _set_extent(self, value):
        self.extent = set_property(self.extent, value)

    def _set_solidity(self, value):
        self.solidity = set_property(self.solidity, value)

    def _set_eq_diameter(self, value):
        self.equivalent_diameter = set_property(self.equivalent_diameter, value)

    def _set_orientation(self, value):
        self.orientation = set_property(self.orientation, value)

    @classmethod
    def compare(cls, template, prop):
        res = []
        for attr in sorted(template.__dict__.keys()):
            minimum = template.__dict__[attr][0]
            maximum = template.__dict__[attr][1]
            value = prop.__dict__[attr]

            spread = maximum - minimum
            middle = (maximum + minimum) / 2.0

            similarity = 1 - (abs(value - middle)) / middle

            res.append(similarity)

        return sum(res) / len(res)

