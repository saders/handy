from inspect import currentframe, getframeinfo
import cv2
import numpy as np


def logger(message, frame=currentframe()):
    frameinf = getframeinfo(frame)
    print("{}:{}:{}:{}".format(frameinf.filename, frameinf.function, frameinf.lineno, message))


def print_gesture(image, gesture):
    cv2.putText(image, gesture, (370, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, 255)


def print_gesture_log(gesture, msg):
    print("{}: {}".format(gesture, msg))
