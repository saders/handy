from enum import Enum

import cv2
import numpy as np
from handlers.analyzer import Analyzer
import random
from classes.logger import *
import time


class Drawer:
    color = (255, 0, 0, 255)
    point_size = 10
    __transparent = (255, 0, 0, 0)
    __drawing_mode = False
    __drawing = False
    __coords = (0, 0)
    __mouse_handling = True

    class Gestures(Enum):
        ENABLED = 1
        DISABLED = 0

    def __init__(self, window_name, window_size, mode=Analyzer.Mode.NoAnalysis, gestures=Gestures.DISABLED):
        self.__window_name = window_name
        cv2.namedWindow(window_name)
        self.toggle_handlers()
        self.window_size = window_size
        self.__img = np.zeros((window_size[0], window_size[1], 4), np.uint8)
        self.__current_frame = self.__img
        self.__mode = mode
        self.__gestrues = gestures
        self.__gesture = None

    def update(self, frame=None):
        if frame is None:
            self.__current_frame = self.__overlay(Analyzer().fetch(self.__mode))
        else:
            self.__current_frame = self.__overlay(Analyzer().analyze(frame, self.__mode))

        self.__display_gesture()
        cv2.imshow(self.__window_name, self.__current_frame)

    def __display_gesture(self):
        if self.__gesture and time.time() - self.__gesture[1] < 3:
            print_gesture(self.__current_frame, self.__gesture[0])
        else:
            self.__gesture = None

    def toggle_mode(self):
        self.__drawing_mode = not self.__drawing_mode

    def toggle_handlers(self):
        if self.__mouse_handling:
            cv2.setMouseCallback(self.__window_name, self.__mouse_handler)
        else:
            cv2.setMouseCallback(self.__window_name, self.__gesture_handler)

    def fetch_frame(self):
        return self.__current_frame, self.__window_name

    def __mouse_handler(self, event, x, y, flags=None, param=None):
        self.__coords = (x, y)
        if event == cv2.EVENT_LBUTTONDOWN:
            self.__drawing = True
            self.__draw()
        elif event == cv2.EVENT_MOUSEMOVE:
            if self.__drawing:
                self.__draw()
        elif event == cv2.EVENT_LBUTTONUP:
            self.__drawing = False
            self.__draw()

    def gesture_handler(self, event, x, y):
        if self.__gestrues == self.Gestures.DISABLED:
            pass
        elif event == "full_hand":
            cv2.circle(self.__img, (x, y), self.point_size, self.color, -1)
            msg = "drawing at: {} {}".format(x, y)
        elif event == "victoria":
            cv2.circle(self.__img, (x, y), self.point_size, self.__transparent, -1)
            msg = "erasing at: {} {}".format(x, y)
        elif event == "fist":
            b = random.randint(0, 255)
            g = random.randint(0, 255)
            r = random.randint(0, 255)
            Drawer.color = (b, g, r, 255)
            msg = "new color: {}".format(self.color.__str__())
        elif event == "ok":
            msg = "{}: saving.".format(event)
        else:
            msg = "Gesture not yet implemented!"

        print_gesture_log(event, msg)
        self.__gesture = (event, time.time())

    def __gesture_handler(self, event, x, y, flags=None, param=None):
        print("gestures not yet implemented!")

    def __draw(self):
        if self.__drawing_mode:
            self.__draw_colored()
        else:
            self.__draw_transparent()

    def __draw_colored(self):
        cv2.circle(self.__img, self.__coords, self.point_size, self.color, -1)

    def __draw_transparent(self):
        cv2.circle(self.__img, self.__coords, self.point_size, self.__transparent, -1)

    def __overlay(self, background_img):
        bg_img = background_img.copy()
        b, g, r, a = cv2.split(self.__img)
        overlay_color = cv2.merge((b, g, r))

        mask = cv2.medianBlur(a, 5)
        h, w, _ = overlay_color.shape

        roi = bg_img[0:h, 0:w]
        img1_bg = cv2.bitwise_and(roi.copy(), roi.copy(), mask=cv2.bitwise_not(mask))
        img2_fg = cv2.bitwise_and(overlay_color, overlay_color, mask=mask)
        bg_img[0:h, 0:w] = cv2.add(img1_bg, img2_fg)
        return bg_img

