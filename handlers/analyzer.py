from enum import Enum
import cv2
import numpy as np
from collections import deque
from classes import singleton

from classes.db.database import DataBase
from classes.db.properties import Properties


class Analyzer(object):
    __metaclass__ = singleton.Singleton
    database = None
    db = DataBase()

    class Mode(Enum):
        NoAnalysis = 0
        Color = 1
        Binary = 2
        Contour = 3

    window_size = (480, 640)
    __frames = {Mode.NoAnalysis: np.zeros((window_size[0], window_size[1]), np.uint8),
                Mode.Color: np.zeros((window_size[0], window_size[1]), np.uint8),
                Mode.Binary: np.zeros((window_size[0], window_size[1]), np.uint8),
                Mode.Contour: np.zeros((window_size[0], window_size[1]), np.uint8)}
    __current_frame = np.zeros((window_size[0], window_size[1], 4), np.uint8)
    __background = cv2.createBackgroundSubtractorMOG2(0, 100)
    __kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    __recognizer_mode = Mode.Color
    __trace = deque()
    __gesture = ()
    __backgroundSub = cv2.createBackgroundSubtractorKNN(history=4000, dist2Threshold=400)
    __pixels = 0.0

    def init_size(self, window_size):
        self.window_size = window_size
        self.__pixels = float(window_size[0] * window_size[1])
        self.__frames = {Analyzer.Mode.NoAnalysis: np.zeros((window_size[0], window_size[1]), np.uint8),
                         Analyzer.Mode.Color: np.zeros((window_size[0], window_size[1]), np.uint8),
                         Analyzer.Mode.Binary: np.zeros((window_size[0], window_size[1]), np.uint8),
                         Analyzer.Mode.Contour: np.zeros((window_size[0], window_size[1]), np.uint8)}
        self.__current_frame = np.zeros((window_size[0], window_size[1], 4), np.uint8)

    def analyze(self, frame, mode=Mode.NoAnalysis):
        self.__frames[Analyzer.Mode.NoAnalysis] = frame.copy()

        mask, thresh = self.binarize(frame)
        self.find_contours(frame, thresh)

        self.__frames[Analyzer.Mode.Color] = frame
        self.__frames[Analyzer.Mode.Binary] = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
        return self.fetch(mode)

    def binarize(self, frame):
        frame_blurred = cv2.GaussianBlur(frame, (5, 5), 300)
        mask = self.__backgroundSub.apply(frame_blurred)
        mask = cv2.inRange(mask, 127, 255)

        change = np.sum(mask == 255) / self.__pixels
        if 0.95 > change > 0.5:
            self.__backgroundSub = cv2.createBackgroundSubtractorKNN(history=4000, dist2Threshold=400)

        _, thresh = cv2.threshold(mask, 40, 255, 0)
        return mask, thresh

    def find_contours(self, frame, thresh):
        im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        if len(contours) != 0:
            c = max(contours, key=cv2.contourArea)
            if cv2.contourArea(c) > 15000:
                try:
                    self.find_gesture(thresh, c)
                except Exception as e:
                    logger(e.message, currentframe())

            x, y, w, h = cv2.boundingRect(c)
            center_x = x + w / 2
            center_y = y + h / 2
            if h * w > 6300:
                cv2.circle(frame, (center_x, center_y), max(w / 2, h / 2), (0, 255, 0), 5)
                self.__trace.append((center_x, center_y))
                cv2.polylines(frame, np.int32([self.__trace]), False, (0, 0, 255), 3)
                if len(self.__trace) > 5:
                    self.__trace.popleft()

            contours = np.zeros((self.window_size[0], self.window_size[1], 3), np.uint8)
            cv2.drawContours(contours, c, -1, (255, 255, 255), 2)
            self.__frames[Analyzer.Mode.Contour] = contours


    def find_gesture(self, img, contour):
        x, y, w, h = cv2.boundingRect(contour)
        hand = img[y:y + h, x:x + w]
        hand = cv2.morphologyEx(hand, cv2.MORPH_CLOSE, np.ones((5, 5), np.uint8))

        prop = Properties()
        prop.aspect_radio = w / float(h)

        area = cv2.contourArea(contour)
        prop.extent = area / float(w * h)

        convex_hull = cv2.convexHull(contour)
        hull_area = cv2.contourArea(convex_hull)
        prop.solidity = area / float(hull_area)

        prop.equivalent_diameter = np.math.sqrt(4 * area / np.math.pi)

        _, _, angle = cv2.fitEllipse(contour)
        prop.orientation = angle

        center_x = x + w / 2
        center_y = y + h / 2

        Analyzer.__gesture = (DataBase.find_match(prop), center_x, center_y)

    def fetch(self, mode=Mode.NoAnalysis):
        return self.__frames[mode]

    @classmethod
    def fetch_gesture(cls):
        return cls.__gesture
