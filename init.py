import Tkinter as tk
import threading
import time
import datetime

from classes.db.database import DataBase
from handlers.drawer import *
from handlers.analyzer import *
import os
import tkFileDialog
from pprint import pprint

LOOPING = True
IDLE_STATE = 'idle'
OPEN_STATE = 'open'
SAVE_STATE = 'save'
SERIES_STATE = 'series'
DRAW_STATE = 'edit'
OUTPUT = "output/"
EXIT_KEY = 27
OPEN_FILES = None
GESTURE = None


class State:
    states = [IDLE_STATE, OPEN_STATE, SAVE_STATE, SERIES_STATE, DRAW_STATE]

    def __init__(self):
        self.current_state = self.previous_state = IDLE_STATE

    def set_state(self, st):
        if st in self.states:
            self.previous_state = self.current_state
            self.current_state = st


state = State()


class MasterWindow:
    def __init__(self, master):
        self.master = master
        self.init_button_window()

    def open_image(self):
        global OPEN_FILES
        files = tkFileDialog.askopenfilenames(initialdir=".", title="Select file",
                                              filetypes=(("all files", "*.*"),))
        OPEN_FILES = self.master.tk.splitlist(files)
        print(OPEN_FILES)
        if len(OPEN_FILES) > 0:
            state.set_state(OPEN_STATE)

    def save_image(self):
        state.set_state(SAVE_STATE)

    def save_series(self):
        state.set_state(SERIES_STATE)

    def edit_image(self):
        if state.current_state == DRAW_STATE:
            state.set_state(IDLE_STATE)
        else:
            state.set_state(DRAW_STATE)

    def init_button_window(self):
        self.frame = tk.Frame(self.master)
        self.open_button = tk.Button(self.frame, text='Open', width=25, command=self.open_image)
        self.save_button = tk.Button(self.frame, text='Save', width=25, command=self.save_image)
        self.series_button = tk.Button(self.frame, text='Save series', width=25, command=self.save_series)
        self.edit_button = tk.Button(self.frame, text='Draw/Erase', width=25, command=self.edit_image)
        self.open_button.pack()
        self.save_button.pack()
        self.series_button.pack()
        self.edit_button.pack()
        self.frame.pack()


class OpenCv:
    size = (480, 640)

    def __init__(self):
        self.cap = cv2.VideoCapture(0)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, self.size[0])
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, self.size[1])
        Analyzer().init_size(self.size)
        self.stack = []
        self.series_flag = False
        self.opened_iterator = None
        self.gestures = False
        self.opened_frame = False
        self.windows = [Drawer("main", self.size, gestures=Drawer.Gestures.ENABLED),
                        Drawer("Binary", window_size=self.size, mode=Analyzer.Mode.Binary),
                        # Drawer("Canny", window_size=self.size, mode=Analyzer.Mode.Contour),
                        Drawer("Hand", window_size=self.size, mode=Analyzer.Mode.Color)]
        self.run()

    def run(self):
        global LOOPING
        global state
        while LOOPING:
            self.send_frames()
            self.handle_state()
            self.handle_keyboard(state.current_state)
            self.handle_gestures()
        self.cap.release()
        cv2.destroyAllWindows()
        print("Is Kill")

    def handle_keyboard(self, current_state):
        global LOOPING
        k = cv2.waitKey(5) & 0xFF
        if current_state == DRAW_STATE and k == ord('m'):
            for window in self.windows:
                try:
                    window.toggle_mode()
                except:
                    continue
        elif k == EXIT_KEY:
            LOOPING = False
        elif self.opened_frame and k == ord(','):
            self.prev_image()
        elif self.opened_frame and k == ord('.'):
            self.next_image()
        elif k == ord('g'):
            self.toggle_gestures()
        elif k == ord('r'):
            self.rollback_from_open_files()
        elif k == ord('h'):
            self.print_help()

    def toggle_gestures(self):
        self.gestures = not self.gestures
        print("Gestures: {}".format(self.gestures))

    def handle_gestures(self):
        gesture, x, y = Analyzer.fetch_gesture()
        if gesture and self.gestures:
            # logger("Found gesture {}".format(gesture), currentframe())
            if "ok" == gesture:
                state.set_state(SAVE_STATE)

            self.windows[0].gesture_handler(gesture, x, y)

    def handle_state(self):
        s = state.current_state
        if s == SAVE_STATE:
            self.save_frames()
        elif s == SERIES_STATE:
            self.save_series()
        elif s == DRAW_STATE:
            self.drawing_mode()
        elif s == OPEN_STATE:
            self.open_mode()
            print(state.current_state)

    def save_frames(self):
        print("Saving")
        for window in self.windows:
            TS = datetime.datetime.fromtimestamp(time.time()).strftime('%Y_%m_%d-%H_%M_%S')
            frame, name = window.fetch_frame()
            cv2.imwrite(OUTPUT + TS + "_out_{}.png".format(name), frame)
        state.set_state(IDLE_STATE)

    def save_series(self):
        if len(self.stack) >= len(self.windows) * 5:
            print("Saving series")
            iterator = 0
            while len(self.stack):
                TS = datetime.datetime.fromtimestamp(time.time()).strftime('%Y_%m_%d-%H_%M_%S')
                for frame in range(0, len(self.windows)):
                    (frame, name) = self.stack.pop(0)
                    cv2.imwrite(OUTPUT + TS + "_out_{}_{}.png".format(name, iterator), frame)
                iterator += 1
            self.stack = []
            state.set_state(IDLE_STATE)
        else:
            for window in self.windows:
                frame, name = window.fetch_frame()
                self.stack.append((frame, name))

    def send_frames(self):
        ret = frame = None
        if self.opened_frame:
            frame = self.control_frame()
            if frame is None:
                print("ERROR!: Could not read the image.")
                frame = self.rollback_from_open_files()
        else:
            ret, frame = self.cap.read()
            Analyzer().analyze(frame)
        for window in self.windows:
            window.update()

    def rollback_from_open_files(self):
        global OPEN_FILES
        print("Rollback!")
        self.opened_frame = False
        self.opened_iterator = None
        OPEN_FILES = None
        ret, frame = self.cap.read()
        return frame

    def drawing_mode(self):
        for window in self.windows:
            try:
                window.toggle_mode()
            except:
                continue

        state.set_state(IDLE_STATE)

    def open_mode(self):
        global OPEN_FILES
        if OPEN_FILES:
            self.opened_frame = True
        else:
            self.opened_frame = False
        state.set_state(IDLE_STATE)

    def control_frame(self):
        if self.opened_iterator is None or self.opened_iterator < 0 or self.opened_iterator >= len(OPEN_FILES):
            self.opened_iterator = 0
        return cv2.imread(OPEN_FILES[self.opened_iterator], 1)

    def print_help(self):
        print("*" * 20)
        pprint(vars(self))
        print("OPEN FILES: ", OPEN_FILES)
        print("STATE: ", state.current_state)

    def prev_image(self):
        print("PREV IMG", self.opened_iterator)
        if self.opened_iterator > 0:
            self.opened_iterator -= 1

    def next_image(self):
        print("NEXT IMG", self.opened_iterator)
        if self.opened_iterator < len(OPEN_FILES):
            self.opened_iterator += 1


def opencv_worker():
    opencv = OpenCv()


def main():
    global LOOPING
    DataBase.read()
    DataBase.print_database()
    print(
        """Key usage
    h - display help and values of OpenCv object
    r - to return to camera capture
    g - to toggle gesture handling
    <,> - to swap between images
    esc - to exit
""")
    try:
        os.mkdir("output")
    except:
        pass
    root = tk.Tk()
    app = MasterWindow(root)
    t = threading.Thread(target=opencv_worker, args=())
    t.start()
    while LOOPING:
        try:
            root.update_idletasks()
            root.update()
        except Exception as e:
            logger(e.message, currentframe())
            break
    LOOPING = False


if __name__ == '__main__':
    main()
